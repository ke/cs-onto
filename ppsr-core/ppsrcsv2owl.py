#
# Generating an OWL version of the ppsr core attributes
#
# principles
#
# we start from a CSV file with entity; attr or entity name; description; data of entity type; obligation; multiplicity; synonym terms
#
# translation patterns:

# e; a; d; Date Time/Text/Vocabulary/mediaFile/decimal; o; min:max; s

# :a rdf:type owl:DatatypeProperty
# :e rdfs:subClassOf [rdf:type owl:Restriction ; owl:onProperty :a ; owl:allValuesFrom xsd:DateTime]
# if min>0 : :e rdfs:subClassOf :a min min xsd:DateTime
# if max ≠ n: :e rdfs:subClassOf :a max max xsd:DateTime


# e; a; d; Class; o; min:max; s

# a rdf:type owl:ObjectProperty
# e rdfs:subClassOf [ rdf:type owl:Restriction ; owl:onProperty :a ; owl:allValuesFrom :a ]


# e; a; d; Vocabulary; o; min:max; s
# :e rdfs:subClassOf 
#         [ rdf:type owl:Restriction ; owl:onProperty :a ;
#           owl:someValuesFrom [ rdf:type rdfs:Datatype ;
#                                  owl:oneOf ("v1", "v2", ..., "vn") ]]

import sys
from typing import Dict, Set
import csv

ontoURI = 'http://cui.unige.ch/isi/onto/ppsr.owl'

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def gen_only(c: str, prop: str, d: str) :
    allRestr = c + ' rdf:type owl:Class ; rdfs:subClassOf [ rdf:type owl:Restriction ; owl:onProperty ' + prop + '; owl:allValuesFrom ' + d + '] . \n'
    print(allRestr)

def gen_dataprop(p: str) :
    print(':'+p+' rdf:type owl:DatatypeProperty .\n')

def gen_objprop(p: str) :
    print(':'+p+' rdf:type owl:ObjectProperty .\n')

def prefixes() :
    
    print(f"""
@prefix : <{ontoURI}#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .

@base <{ontoURI}> .
""")

    print(f"""<{ontoURI}> rdf:type owl:Ontology .""")


ifileName = sys.argv[1]
ofileName = sys.argv[2]
print('Output to ' + ofileName)
sys.stdout = open(ofileName, 'w')

csvfile = open(ifileName, newline='')
rdr = csv.reader(csvfile, delimiter=',',quotechar='"')

prefixes()

for row in rdr:
    # eprint(row)
    # eprint('---\n')
    
    (c_entity, c_attr_or_entity, c_description, c_type, c_obligation, c_multiplicity, c_synonyms) = row[0:7]

    c_entity = c_entity.strip()
    c_attr_or_entity = c_attr_or_entity.strip()
    c_type = c_type.strip()
    c_multiplicity = c_multiplicity.strip()

    if c_type.lower() == "class" :
        gen_objprop(c_attr_or_entity)
        gen_only(':'+c_entity, ':'+c_attr_or_entity, ':'+c_attr_or_entity)
    elif c_type != '':
        dtype = ':'+c_type
        if c_type.lower() == "text" or c_type.lower() == "vocabulary": dtype = 'xsd:string'
        if c_type.lower() == "date time"  or c_type.lower() == 'time': dtype = 'xsd:dateTime'
        if c_type.lower() == "decimal" : dtype = 'xsd:decimal'
        if c_type.lower() == "integer" : dtype = 'xsd:integer'
        if c_type.lower() == "boolean" : dtype = 'xsd:boolean'
        if c_type.lower() == 'uri' or c_type.lower() == 'url' : dtype = 'xsd:anyURI'
        if dtype.startswith(':') : eprint('*** |'+c_type+'|')
        gen_dataprop(c_attr_or_entity)
        gen_only(':'+c_entity, ':'+c_attr_or_entity, dtype)

