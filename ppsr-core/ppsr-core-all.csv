,,,,,,
,,,,,,
activity,class,"An activity is analagous to a survey and comprises 2 components: a metadata schema; and an observational data model (ie. the data schema into which observational records are created). The data schema definition will represent a specific data collection protocol. In the context of an activity/survey, these exist as a singular pair of objects. Usage of an activity is always made in the context of an event, ie. A non-persistent time-based usage of an observational data schema. Observational data schemas are domain and protocol specific, and may be published in other repositories.",Class,Optional,0:n,dcmitype:Event
activity,activityId,A globally unique identifier for an activity.,text,Mandatory,1:1,
activity,datasetMetadataSchema,The datasetMetadataSchema (DMM) describes the metadata pertaining to the specific observationalDataSchema selected and it's associated data. There is a 1:1 relationship between the datasetMetadataSchema and the observationalDataSchema. The datasetMetadataSchema is consistent for all classes of observationalDataDomains. This is a class object.,Class,Mandatory,1:1,
datasetMetadataSchema,dmmCoreTerms,The set of core terms which comprise the PPSR-Core Dataset Metadata Model (DMM). These are the minimum set of attributes required to adequately describe a dataset and enable exchange of dataset metadata between data catalogues.,Class,Optional,1:1,dcat:CatalogRecord
dmmCoreTerms,dcterms:identifier,Persistent identifier of a dataset (associated with the project). Should equate to the datasetExternalId if data is stored in an external repository.,text,Mandatory,1:1,"dwcterms:datasetID
cosi:hasIdentifierValue"
dmmCoreTerms,dcterms:dateSubmitted,The date a dataset submission was published into a receiving system. Uses the ISO 8601:2004 (E) dateTime standard,date time,Mandatory,1:1,"prov:generatedAtTime
CI_Citation date"
dmmCoreTerms,dcterms:modified,"The most recent dateTime at which the resource was changed. Uses the ISO 8601:2004 (E) dateTime standard"",",date time,Mandatory,1:1,prov:generatedAtTime
dmmCoreTerms,datasetStatus,Indicator of the current status of a dataset (e.g. if it already published),vocabulary,Mandatory,1:1,cosi:hasStatus
dmmCoreTerms,dcterms:title,The name of the dataset for discovery and citation purposes.,text,Mandatory,1:1,"dwcterms:datasetName
CI_Citation.title
cosi:hasTitle"
dmmCoreTerms,dcterms:abstract,Abstract or description of the dataset.,text,Mandatory,1:1,cosi:hasDescription
dmmCoreTerms,dcterms:accessRights,Category of rights to use IP contained in the dataset or a type of use applied to the dataset.,vocabulary,Mandatory,1:1,dct:rights
dmmCoreTerms,dcterms:bibliographicCitation,Format to be structured as follows: 'Author/Rightsholder. (Year). Title of data set (Version number) [Description of form]. Retrieved from https://<website url>'. The attribution text string to be cited by people who use the dataset.,text,Optional,0:1,
dmmCoreTerms,dcterms:rightsHolder,The name of the organisation which is the legal custodian of the dataset.,text,Mandatory,1:1,prov:wasAttributedTo
dmmCoreTerms,dcterms:license,License applied to the dataset.,vocabulary,Mandatory,1:1,cosi:hasLicenceInformation
dmmCoreTerms,dcterms:language,The machine language the dataset and associated metadata is encoded in. Uses Unicode Standard UTF-8 (ISO/IEC 10646:2014 plus Amendment 1).,text,Optional,0:n,MD_DataIdentification.characterSet
dmmCoreTerms,datesetStartDate,The date on which the dataset collection survey commences. This may reflect the earliest record in the dataset or when a survey is open to begin data recording. This date may be => the projectStartDate. Uses the ISO 8601:2004 (E) dateTime standard.,date time,Mandatory,1:1,
dmmCoreTerms,datasetEndDate,The date on which the dataset collection survey concluded. Uses the ISO 8601:2004 (E) dateTime standard.,date time,Optional,0:1,
dmmCoreTerms,methodType,The type of methodology or sampling protocol used to collect the dataset.,vocabulary,Mandatory,1:1,
dmmCoreTerms,dataAccessMethod,A list of available methods for people to access the dataset.,vocabulary,Mandatory,1:n,
datasetMetadataSchema,methodSpecification,Details of the methodology or sampling protocol used to collect the dataset.,Class,Mandatory,1:1,cosi:hasRelatedMaterial
methodSpecification,samplingProtocolDomain,The name of the methodology or sampling protocol used to collect the dataset.,vocabulary,Optional,0:1,
methodSpecification,samplingProtocolMethod,The sampling protocol method used for a given survey.,vocabulary,Optional,0:1,"dcterms:samplingProtocol
dwcterms:samplingProtocol
cosi:hasProcedure"
methodSpecification,methodAbstract,Description of the methodology or sampling protocol used to collect the dataset.,text,Optional,0:1,
methodSpecification,methodUrl,URL address of an officially published article which describes the methodology or sampling protocol used to collect the dataset.,url,Optional,0:1,
methodSpecification,methodDocUrl,URL link to an uploaded document artefact which describes the methodology or sampling protocol used to collect the dataset.,url,Optional,0:1,
datasetMetadataSchema,observationalDataModel,"The observationalDataDomain contains an array of different domain schemas (eg. biodiversity, water, atmosphere, ecology, geology, geomorphology, astronomy, etc.). Each domain will contain an array of standard protocols which apply in that domain context. The domains listed are not a comprehensive list and are expected to be appended to over time as new domains are specified and appropriate samplingProtocol standards are defined for them. 
This class object serves only to structurally differentiate and describe the different domains and is not a structural element of the observationalDataModel (ODM).",Class,Mandatory,1:1,dcat:Dataset
,,,,,,
,,,,,,
,,,,,,
Project,projectId,Globally unique identifier (GUID) for the project. System generated.,Text,Optional,0:1,"dcterms:identifier
cosi:hasIdentifier"
datasetMetadataSchema,dmmExtensionTerms,The set of extension terms which comprise the PPSR-Core Dataset Metadata Model (DMM). These terms enhance the description of a dataset and improve the ability of users of the dataset to understand or interpret fitness for use.,Class,Optional,0:1,dcat:CatalogRecord
dmmExtensionTerms,datasetUpdateFrequency,How often the dataset is updated.,vocabulary,Optional,0:1,dcterms:accrualPeriodicity
dmmExtensionTerms,datasetExternalUrl,Web location where the dataset will be published.,text,Optional,0:n,
dmmExtensionTerms,dcat:downloadURL,A URL from which dataset observation records can be accessed and downloaded.,url,Optional,0:1,
dmmExtensionTerms,datasetGeographicCoverage,Geographic/spatial scope of coverage of the collection sites of data records within the dataset. Uses OGC GeoAPI (09-083r3) standard.,geoObject,Optional,0:n,
dmmExtensionTerms,cosi:hasHypothesis,The experimental hypothesis underpinning the experimental design for which the dataset was collected.,text,Optional,0:1,
dmmExtensionTerms,cosi:hasInstrument,Details of instrumentation used in the data recording.,text,Optional,0:n,
dmmExtensionTerms,dataQualityAssuranceMethod,"Description of the types of data quality assurance methods that were applied in capturing, curating and managing the dataset.",vocabulary,Optional,0:n,
dmmExtensionTerms,dataQualityAssuranceDescription,Detailed description of the methods used to quality assure the dataset both during capture and post processing. This is important for data users to understand the processes applied to the data to verify or enhance it's quality for use.,text,Optional,0:1,
dmmExtensionTerms,usageGuide,"Description of any constraints and biases in the dataset which are associated with how the data collection methodology was applied, eg. Concentration of data points along access networks, targeted/non-random approaches causing bias towards certain factors at the expense of other factors, etc.",text,Optional,0:1,
dmmExtensionTerms,activityCount,Number of data recording events in the dataset,integer,Optional,0:1,
dmmExtensionTerms,datasetAssociatedMedia,Image(s) and/or other media used to graphically enhance or represent the dataset. This is a class object.,Class,Optional,0:n,
datasetAssociatedMedia,datasetAssociatedMediaType,The category of media type representing the type of dataset media item chosen,vocabulary ,Mandatory,1:1,foaf:img
datasetAssociatedMedia,datasetAssociatedMediaFile,Media file upload representing the type of dataset media chosen,mediaFile,Mandatory,1:1,foaf:img
datasetAssociatedMedia,datasetAssociatedMediaCredit,Attribution credit for the logo image or other media,text,Mandatory,1:1,dcterms:bibliographicCitation
dmmExtensionTerms,dataAccuracyDeclarations,Generalised categories that best reflect the accuracy of records in the dataset.,Class,Optional,0:4,
dataAccuracyDeclarations,spatialAccuracy,A generalised category that best reflects the least spatially accurate record in the dataset.,vocabulary,Optional,0:1,
dataAccuracyDeclarations,temporalAccuracy,A generalised category that best reflects the least accurate record in the dataset in respect to date of the observation.,vocabulary,Optional,0:1,
dataAccuracyDeclarations,speciesIdentificationAccuracy,A generalised category that best reflects the least accurate record in the dataset for species identification. Choose 'Not applicable' species fields are not included in the dataset.,vocabulary,Optional,0:1,
dataAccuracyDeclarations,nonTaxonomicAccuracy,A generalised category that best reflects the least accurate record in the dataset in respect to non-biodiversity attributes.,vocabulary,Optional,0:1,
dmmExtensionTerms,dataManagementPlan,Details of a data management plan associated with the dataset.,Class,Optional,0:1,cosi:hasRelatedMaterial
dataManagementPlan,isDataManagementPolicyDocumented,Indicator of whether a data management plan has been prepared for the dataset.,boolean,Mandatory,1:1,
dataManagementPlan,dataManagementPolicyDescription,Description of data management policy,text,Optional,0:1,
dataManagementPlan,dataManagementPolicyURL,Link to data management policy description,url,Optional,0:1,
dataManagementPlan,dataManagementPolicyDocument,Document describing data management policy,url,Optional,0:1,
dataManagementPlan,dataManagementPrinciplesConformance,Assessment of the conformance of the data management principles applied to the dataset with standard GEOlabels.,text,Optional,0:1,
,,,,,,
,,,,,,
Project,projectId,Globally unique identifier (GUID) for the project. System generated.,Text,Optional,0:1,"dcterms:identifier
cosi:hasIdentifier"
datasetMetadataSchema,dmmExtensionTerms,The set of extension terms which comprise the PPSR-Core Dataset Metadata Model (DMM). These terms enhance the description of a dataset and improve the ability of users of the dataset to understand or interpret fitness for use.,Class,Optional,0:1,dcat:CatalogRecord
dmmExtensionTerms,datasetUpdateFrequency,How often the dataset is updated.,vocabulary,Optional,0:1,dcterms:accrualPeriodicity
dmmExtensionTerms,datasetExternalUrl,Web location where the dataset will be published.,text,Optional,0:n,
dmmExtensionTerms,dcat:downloadURL,A URL from which dataset observation records can be accessed and downloaded.,url,Optional,0:1,
dmmExtensionTerms,datasetGeographicCoverage,Geographic/spatial scope of coverage of the collection sites of data records within the dataset. Uses OGC GeoAPI (09-083r3) standard.,geoObject,Optional,0:n,
dmmExtensionTerms,cosi:hasHypothesis,The experimental hypothesis underpinning the experimental design for which the dataset was collected.,text,Optional,0:1,
dmmExtensionTerms,cosi:hasInstrument,Details of instrumentation used in the data recording.,text,Optional,0:n,
dmmExtensionTerms,dataQualityAssuranceMethod,"Description of the types of data quality assurance methods that were applied in capturing, curating and managing the dataset.",vocabulary,Optional,0:n,
dmmExtensionTerms,dataQualityAssuranceDescription,Detailed description of the methods used to quality assure the dataset both during capture and post processing. This is important for data users to understand the processes applied to the data to verify or enhance it's quality for use.,text,Optional,0:1,
dmmExtensionTerms,usageGuide,"Description of any constraints and biases in the dataset which are associated with how the data collection methodology was applied, eg. Concentration of data points along access networks, targeted/non-random approaches causing bias towards certain factors at the expense of other factors, etc.",text,Optional,0:1,
dmmExtensionTerms,activityCount,Number of data recording events in the dataset,integer,Optional,0:1,
dmmExtensionTerms,datasetAssociatedMedia,Image(s) and/or other media used to graphically enhance or represent the dataset. This is a class object.,Class,Optional,0:n,
datasetAssociatedMedia,datasetAssociatedMediaType,The category of media type representing the type of dataset media item chosen,vocabulary ,Mandatory,1:1,foaf:img
datasetAssociatedMedia,datasetAssociatedMediaFile,Media file upload representing the type of dataset media chosen,mediaFile,Mandatory,1:1,foaf:img
datasetAssociatedMedia,datasetAssociatedMediaCredit,Attribution credit for the logo image or other media,text,Mandatory,1:1,dcterms:bibliographicCitation
dmmExtensionTerms,dataAccuracyDeclarations,Generalised categories that best reflect the accuracy of records in the dataset.,Class,Optional,0:4,
dataAccuracyDeclarations,spatialAccuracy,A generalised category that best reflects the least spatially accurate record in the dataset.,vocabulary,Optional,0:1,
dataAccuracyDeclarations,temporalAccuracy,A generalised category that best reflects the least accurate record in the dataset in respect to date of the observation.,vocabulary,Optional,0:1,
dataAccuracyDeclarations,speciesIdentificationAccuracy,A generalised category that best reflects the least accurate record in the dataset for species identification. Choose 'Not applicable' species fields are not included in the dataset.,vocabulary,Optional,0:1,
dataAccuracyDeclarations,nonTaxonomicAccuracy,A generalised category that best reflects the least accurate record in the dataset in respect to non-biodiversity attributes.,vocabulary,Optional,0:1,
dmmExtensionTerms,dataManagementPlan,Details of a data management plan associated with the dataset.,Class,Optional,0:1,cosi:hasRelatedMaterial
dataManagementPlan,isDataManagementPolicyDocumented,Indicator of whether a data management plan has been prepared for the dataset.,boolean,Mandatory,1:1,
dataManagementPlan,dataManagementPolicyDescription,Description of data management policy,text,Optional,0:1,
dataManagementPlan,dataManagementPolicyURL,Link to data management policy description,url,Optional,0:1,
dataManagementPlan,dataManagementPolicyDocument,Document describing data management policy,url,Optional,0:1,
dataManagementPlan,dataManagementPrinciplesConformance,Assessment of the conformance of the data management principles applied to the dataset with standard GEOlabels.,text,Optional,0:1,
,,,,,,
,,,,,,
project,projectExternalId,The identifier of the project in an external database or repository..,text,Optional,0:n,"dcterms:publisher
prov:wasAttributedTo"
project,cosi:belongsToProgramme,Name of the Programme or campaign that the project is associated with.,text,Optional,0:1,cosi:belongsToProgramme
project,projectOriginalRepository,The name of the project database in which a project was first registered or published. Allows traceability of a project in multiple databases to it's original registration repository.,text,Optional,0:n,
project,projectHowToParticipate,Free text description of how people can get involved in the project. Textual instructions for joining the project.,text,Optional,0:1,
project,projectTask,Free text description of how people can get involved in the project. Textual instructions for joining the project.,text,Optional,0:1,cosi:hasActivity
project,projectIntendedOutcomes,A set of described outcomes intended to be achieved by the project.,text,Optional,0:n,"proj:objective
cosi:hasOutput"
project,projectOutcome,The outcome(s) achieved or delivered by a project.,text,Optional,0:n,cosi:hasConclusion
project,scientificProcessInvolved,Parts of the process of science that participants exercise,vocabulary,Optional,0:n,cosi:hasProcedure
project,numberOfScientificPublications,Number of peer reviewed publications referencing the project and/or project datasets.,integer,Optional,0:1,
project,numberOfOtherPublications,Number of reports/grey literature publications referencing the project and/or project datasets.,integer,Optional,0:1,
project,projectAdditionalInformation,Supplemental information for any project-specific data administrators want to make available,text,Optional,0:1,cosi:hasRelatedMaterial
project,projectCountry,The country in which the project is being undertaken. This may be multiple countries. Select list of countries. Used for enabling nationalised views of projects in order to make searchability and applicability of projects more nationally relevant for users. Applies the ISO 3166: 2006 standard for country names.,vocabulary,Optional,0:n,
project,projectEquipment,Required or suggested equipment to be used in the project.,text,Optional,0:1,cosi:hasInstrument
project,projectExternalLinks,"URL for links to any external resources associated with a project. These may include links to project mobile apps, social media pages, etc.",uri,Optional,0:n,
project,projectPlannedEndDate,The date that the project is planned to end. Applicable for projects operating over a defined period of time. The date on which the project ended or will end. Uses the ISO 8601:2004 (E) dateTime standard,date time,Optional,0:1,
project,projectPlannedStartDate,The date that the project is planned to commence. The date on which the project began or will begin. Uses the ISO 8601:2004 (E) dateTime standard,date time,Optional,0:1,
project,projectResearchType,"Area of research covered by the project (e.g., ecological research)",vocabulary,Optional,0:1,cosi:hasFieldOfStudy
project,projectScientificCollaborators,List of scientific collaborators; i.e. scientists or academic departments using the data collected,text,Optional,0:n,cosi:hasInvestigator
project,projectAssociatedParty,A party associated with the project with a particular association role. This ia a class object.,Class,Optional,0:n,
projectAssociatedParty,projectAssociatedPartyId,The id of an associated party. This may be a foreign key to the party information stored in another database entity.,text,Optional,0:1,
projectAssociatedParty,projectAssociatedPartyName,Short text name or title of a party (eg. Organisation) associated with the project and performing a role in the project.,text,Optional,0:1,
projectAssociatedParty,projectUsFederalSponsor,Name(s) of US Federal entity(s) providing funding to the project,vocabulary,Optional,0:1,
projectAssociatedParty,projectAssociatedPartyRole,A role which a party (eg. Organisation) associated with the project undertakes. Each associated party listed should have a role.,vocabulary,Optional,0:1,
project,participationFees,An entity for specifying details of participation fees if they are applicable. This is a class object.,Class,Optional,0:1,
participationFees,participationFeeApplicable,Flag to indicate whether participation fees are applicable.,boolean,Mandatory,1:1,
participationFees,participationFeeAmount,The currency amount for a fee to participate in the project.,decimal,Optional,0:1,
project,projectFunding,Type(s) and source(s) of funding contributions to the project. This is a class object.,Class,Optional,0:n,
projectFunding,projectFundingSource,Type of name of the contributor(s)of funding to the project,text,Optional,0:1,
projectFunding,projectFundingSourcePercentageAmount,Percentage of funding contribution for each funding source or funding contributor,decimal,Optional,0:1,
projectFunding,projectFundingSourceCurrencyAmount,Actual amount in local country currency of funding contribution for each funding source or funding contributor,decimal,Optional,0:1,
projectFunding,projectFundingSourceType,Type of funding source(s) contributing to the project. This is a class object.,Class,Mandatory,1:1,
projectFundingSourceType,fundingSourceTypeSubsetA,Funding source type used in Australia.,vocabulary,Optional,0:1,
projectFundingSourceType,fundingSourceTypeSubsetB,Funding source type used in the USA.,vocabulary,Optional,0:1,
project,projectParticipants,Number of project participants. This is a class object.,Class,Optional,0:1,
projectParticipants,projectParticipantsNumberOfActive,Number of active participants based on project's definition of activity,integer,Optional,0:1,
projectParticipants,projectParticipantsTotalRegistered,Total number of registered participants,integer,Optional,0:1,
projectParticipants,projectParticipantAge,The age range classe(s) of participants,vocabulary,Optional,0:n,
,,,,,,
,,,,,,
,,,,,,
programOrCampaign,class,"An overarching context for a set of related projects. Projects may be related in the context of a common funding source, a common thematic or conceptual framework, or other such context. Note that projects do not need to be created in the context of a programOrCampaign, but that a programOrCampaign can be a container for 1:many projects. Entity equates to OWL 2.0 Class object.",,Optional,0:1,
programOrCampaign,programId,"The identifier of the initiative (e.g., overarching research program or funding initiative) encompassing the project"",",text,Mandatory,1:1,
programOrCampaign,programName,"The initiative (e.g., overarching research program) encompassing the project (e.g., Horizon 2020)",text,Mandatory,1:1,
programOrCampaign,programDescription,General description/summary of the Research Program or funding initiative.,text,Optional,0:1,
programOrCampaign,language,The encoding language used for the project. Uses Unicode Standard UTF-8 (ISO/IEC 10646:2014 plus Amendment 1).,vocabulary ,Mandatory,1:1,"dcterms:language
MD_DataIdentification.characterSet"
project,,"A project is an overarching context for a set of associated activities which contribute to a specific objective. Entity equates to OWL 2.0 Class object.
Refer to the PMM Model for details of the project metadata schema.",,Mandatory,1:n,
,,,,,,
,,,,,,
,,,,,,
,,,,,,
,,,,,,
programOrCampaign,project,A project is an overarching context for a set of associated activities which contribute to a specific objective. Entity equates to OWL 2.0 Class object. The project entity is described and defined by this schema.,Class,Mandatory,1:n,
project,projectId,Globally unique identifier (GUID) for the project. System generated.,Text,Mandatory,1:1,"dcterms:identifier
cosi:hasIdentifier"
project,projectDateCreated,The date and time that the record was created in the database. Uses the ISO 8601:2004 (E) dateTime standard,Date Time,Mandatory,1:1,"dcterms:created
prov:generatedAtTime"
project,projectLastUpdatedDate,The date and time that the record was last updated in the database. Uses the ISO 8601:2004 (E) dateTime standard,Date Time,Mandatory,1:1,"dcterms:modified
prov:generatedAtTime"
project,projectName,Short name or title of the project.,text,Mandatory,1:1,"dcterms:title
cosi:hasTitle"
project,projectAim,"The primary aim, goal or objective of the project.",text,Mandatory,1:1,dcterms:abstract
project,projectDescription,Abstract or long description of the project.,text,Mandatory,1:1,"dcterms:description
cosi:hasDescription"
project,hasTag,A set of categorical tags to assist with searching and filtering.,vocabulary ,Optional,0:n,
project,difficultyLevel,Assessed class of project difficulty for untrained participants to undertake. This attribute is used to assist with searching and filtering.,vocabulary ,Optional,0:1,
project,keyword,Keywords (comma separated) which are indexed and aid in searching for and finding projects.,text,Optional,0:n,dct:keyword
project,projectStatus,The activity status of the project.,vocabulary ,Mandatory,1:1,cosi:hasStatus
project,projectStartDate,The actual date that the project began. Uses the ISO 8601:2004 (E) dateTime standard,Date Time,Mandatory,1:1,
project,projectEndDate,The actual date that the project ended. Uses the ISO 8601:2004 (E) dateTime standard,Date Time,Optional,0:1,
project,projectDuration,"The duration or elapsed time over which a project was undertaken. This would typically be derived as the difference in a unit of time between the start and end dates of a project, but may be infinite (if no end date applies) or explicit (if duration is given with no end date specified.",Time,Mandatory,1:1,
project,projectScienceType,he category of science represented by the project. Where the project fits into the landscape of science.,vocabulary ,Mandatory,1:n,cosi:hasFieldOfStudy
project,projectUrl,An HTTP URL to an external web site for the project.,uri,Optional,0:1,
project,unRegions,"Select list based on UN M.49 coding classification, Geographical supranational regions. Used for enabling regionalized views of projects where projects are active, to make searching for projects more regionally relevant for some users.",vocabulary ,Optional,0:n,
project,language,The encoding language used for the project. Uses Unicode Standard UTF-8 (ISO/IEC 10646:2014 plus Amendment 1).,vocabulary ,Mandatory,1:1,"dcterms:language
MD_DataIdentification.characterSet"
project,projectLocality,Other textual location specification,text,Optional,0:1,dwcterms:locality
project,projectResponsiblePartyName,Name of the primary organization responsible for hosting or implementing the project.,text,Mandatory,1:1,"foaf:name
proj:hasLeader
prov:entity"
project,projectResponsiblePartyContactEmail,A contact email address of the party (eg. Organisation) that is responsible for implementing/conducting the project.,text,Mandatory,1:1,
project,contactPoint,The principal contact point for the project. This is a class object.,Class,Mandatory,1:n,dcat:contactPoint
contactPoint,contactName,The name of the agent appointed as the contact for a project.,text,Mandatory,1:n,foaf:name
contactPoint,meansOfContact,The method(s) described for making contact with the project.,vocabulary ,Mandatory,1:n,
contactPoint,contactDetails,Detailed contact information for a project.,text,Mandatory,1:n,
contactPoint,contactPointType,The type of contact point for a project.,vocabulary ,Mandatory,1:n,
project,projectMedia,Image(s) and/or other media used to graphically enhance or represent the project. This is a class object.,Class,Optional,0:n,
projectMedia,projectMediaType,The category of media type representing the type of project media item chosen,vocabulary ,Mandatory,1:1,foaf:img
projectMedia,projectMediaFile,Media file upload representing the type of project media chosen,mediaFile,Mandatory,1:1,foaf:img
projectMedia,projectMediaCredit,Attribution credit for the logo image or other media,text,Mandatory,1:1,dcterms:bibliographicCitation
project,projectGeographicLocation,"User-defined geospatial representation point location or geographic extent of the project. Point locations are typically represented as map pins and extents as polygons. Uses OGC GeoAPI (09-083r3) standard.
This is a class object.",Class,Mandatory,1:n,ISO19107
projectGeographicLocation,projectPinLatitude,"Latitude coordinate of the point location of the project. Typically this is where the project is hosted, e.g., a home institution. Uses OGC GeoAPI (09-083r3) standard.",decimal,Optional,0:1,ISO19107
projectGeographicLocation,projectPinLongitude,"Longitude coordinate of the point location of the project. Typically this is where the project is hosted, e.g. a home institution. Uses OGC GeoAPI (09-083r3) standard.",decimal,Optional,0:1,ISO19107
projectGeographicLocation,projectGeographicCoverage,User-defined geospatial representation(s) of the project area footprint/extent. Coverage is typically represented in a GeoJSON object which has a centroid coordinate ('centre') and a definition of the boundary of the shape. Uses OGC GeoAPI (09-083r3) standard.,geoObject,Optional,0:n,ISO19107
projectGeographicLocation,projectGeographicCoverageCentroidLatitude,Latitude coordinate of the centroid of the project extent area. Latitude coordinate in geographic decimal degrees for the center or home base of the project best representing the project's location as a point. Uses OGC GeoAPI (09-083r3) standard.,decimal,Optional,0:n,ISO19107
projectGeographicLocation,projectGeographicCoverageCentroidLongitude,Longitude coordinate of the centroid of the project extent area. Longitude coordinate in geographic decimal degrees for the center or home base of the project best representing the project's location as a point. Uses OGC GeoAPI (09-083r3) standard.,decimal,Optional,0:n,ISO19107
project,activity,"An activity comprises a schema definition which represents a specific data collection protocol and a schema definition for the metadata associated with all protocols. In the context of an activity, these exist as a singular pair. Usage of an activity is always made in the context of an event, ie. A non-persistent time-based usage of an observational data schema. Observational data schemas are domain and protocol specific, and may be published in other repositories. This is a class object.",Class,Optional,0:n,
